class DeparturesController < ApplicationController
  require 'csv'

  def index
    @departures = read_data('db/Departures.csv').sort_by! { |row| row[:scheduled_time] }
  end

  private

  def read_data(file_path)
    departures = []
    CSV.foreach(file_path, headers: true) do |row|
      departures << {
        timestamp: row['TimeStamp'],
        origin: row['Origin'],
        trip: row['Trip'],
        destination: row['Destination'],
        scheduled_time: formatted_time(row['ScheduledTime']),
        lateness: format_lateness(row['Lateness']),
        track: row['Track'],
        status: row['Status']
      }
    end

    departures
  end

  def formatted_time(stamp)
    stamp == '' ? '' : Time.strptime(stamp,'%s').strftime('%I:%M %p,  %b. %e')
  end

  def format_lateness(delay)
    delay = delay.to_i

    delay == 0 ? '' : (delay / 60.0).ceil.to_s << ' min'
  end
end
